# The MIT License (MIT)
# Copyright (c) 2016 satojkovic

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
import tensorflow as tf
import numpy as np
from six.moves import cPickle as pickle
from six.moves import range
import sys
import os
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.decomposition import PCA



max_steps = 10000
image_width = 64
image_height = 32
num_classes = 27
learning_rate = 0.0001
batch_size = 64
num_channels = 3
patch_size = 5
train_dir = "flickr_logos_27_dataset"


PICKLE_FILENAME = 'deep_logo.pickle'





def reformat(dataset, labels):
    dataset = dataset.reshape((-1, image_width,image_height,
                               num_channels)).astype(np.float32)
    labels = (
        np.arange(num_classes) == labels[:, None]).astype(np.float32)
    return dataset, labels


def train(data, labels):

    lr = LogisticRegression().fit(data, labels)
    yhat = lr.predict(data)

    print("Train Accuracy is:",accuracy_score(labels, yhat))

    return lr


def read_data():
    with open(PICKLE_FILENAME, 'rb') as f:
        save = pickle.load(f)
        train_dataset = save['train_dataset']
        train_labels = save['train_labels']
        test_dataset = save['test_dataset']
        test_labels = save['test_labels']
        del save
        print('Training set', train_dataset.shape, train_labels.shape)
        print('Test set', test_dataset.shape, test_labels.shape)

    return [train_dataset,test_dataset], [train_labels, test_labels]


def main():

    # read input data
    dataset, labels = read_data()

    train_dataset, train_labels = reformat(dataset[0], labels[0])
    test_dataset, test_labels = reformat(dataset[1], labels[1])
    print('Training set', train_dataset.shape, train_labels.shape)
    print('Test set', test_dataset.shape, test_labels.shape)

    train_dataset = train_dataset.reshape((50000,64*32*3))
    test_dataset = test_dataset.reshape((5000, 64 * 32 * 3))
    train_dataset = train_dataset[:20000,:]
    test_dataset = test_dataset[:2000,:]
    print(train_dataset.shape)
    print(test_dataset.shape)
    """pca1 = PCA(n_components=1000)
    pca1.fit(test_dataset)
    test_dataset = pca1.transform(test_dataset)   
    #pca = PCA(n_components=1000)
    pca.fit(train_dataset)
    train_dataset = pca.transform(train_dataset)
 """
    test_labels = np.argmax(test_labels,axis=1)
    train_labels = np.argmax(train_labels, axis=1)
    print(train_dataset.shape)
    print(test_dataset.shape)


    model = train(train_dataset, train_labels[:20000])
    print('Test accuracy: %.1f%%' % accuracy_score(model.predict(test_dataset),test_labels[:2000]))

    


if __name__ == '__main__':
    main()
