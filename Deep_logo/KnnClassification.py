
# coding: utf-8

# In[ ]:


from __future__ import print_function

import numpy as np
import random as rRand
import tensorflow as tf
import pickle
# for dimentionality reduction
from sklearn.decomposition import PCA

FLAGS = tf.app.flags.FLAGS

# global variables
tf.app.flags.DEFINE_integer("image_width", 64, "A width of an input image.")
tf.app.flags.DEFINE_integer("image_height", 32, "A height of an input image.")
tf.app.flags.DEFINE_integer("num_classes", 27, "Number of logo classes.")
tf.app.flags.DEFINE_integer("num_channels", 3, "A number of channels of an input image.")
PICKLE_FILENAME = 'deep_logo.pickle'

def reformat(dataset, labels):
    dataset = dataset.reshape((-1, FLAGS.image_width, FLAGS.image_height, FLAGS.num_channels)).astype(np.float32)
    labels = (np.arange(FLAGS.num_classes) == labels[:, None]).astype(np.float32)
    return dataset, labels

def read_data():
    with open(PICKLE_FILENAME, 'rb') as f:
        save = pickle.load(f)
        train_dataset = save['train_dataset']
        train_labels = save['train_labels']
        valid_dataset = save['valid_dataset']
        valid_labels = save['valid_labels']
        test_dataset = save['test_dataset']
        test_labels = save['test_labels']
        del save

    return [train_dataset, valid_dataset, test_dataset, train_labels, valid_labels, test_labels]


# Read data
Xtr, Xvl, Xte, Ytr, Yvl, Yte = read_data()

# reformat the data
Xtr, Ytr = reformat(Xtr, Ytr)
Xte, Yte = reformat(Xte, Yte)

Xtr = Xtr.reshape((50000, 6144))
Xte = Xte.reshape((5000, 6144))

print(Xtr.shape)
# tf Graph Input

xtr = tf.placeholder(tf.float32, [None, 6144])
ytr = tf.placeholder(tf.float32, [None, 27]) #traning label
xte = tf.placeholder(tf.float32, [6144])


K=3 #how many neighbors
nearest_neighbors=tf.Variable(tf.zeros([K]))

#model
distance = tf.negative(tf.reduce_sum(tf.abs(tf.subtract(xtr, xte)), axis=1)) #L1
# the negitive above if so that top_k can get the lowest distance *_* its a really good hack i learned
values, indices = tf.nn.top_k(distance, k=K, sorted=False)

#a normal list to save
nn = []
for i in range(K):
    nn.append(tf.argmax(ytr[indices[i]], 0)) #taking the result indexes

#saving list in tensor variable
nearest_neighbors=nn

# this will return the unique neighbors the count will return the most common's index
y, idx, count = tf.unique_with_counts(nearest_neighbors)

pred = tf.slice(y, begin=[tf.argmax(count, 0)], size=tf.constant([1], dtype=tf.int64))[0]
# this is tricky count returns the number of repetation in each elements of y and then by begining from that and size begin 1
# it only returns that neighbors value : for example
# suppose a is array([11,  1,  1,  1,  2,  2,  2,  3,  3,  4,  4,  4,  4,  4,  4,  4]) so unique_with_counts of a will
#return y= (array([ 1,  2,  3,  4, 11]) count= array([3, 3, 2, 7, 1])) so argmax of count will be 3 which will be the
#index of 4 in y which is the hight number in a

#setting accuracy as 0
accuracy=0

#initialize of all variables
init=tf.global_variables_initializer()

#start of tensor session
with tf.Session() as sess:

    for i in range(Xte.shape[0] - 3000):
        #return the predicted value
        predicted_value=sess.run(pred,feed_dict={xtr:Xtr, ytr:Ytr, xte:Xte[i,:]})

        print("Test", i, "Prediction", predicted_value, "True Class:", np.argmax(Yte[i]))

        if predicted_value == np.argmax(Yte[i]):
            # if the prediction is right then a double value of 1./200 is added 200 here is the number of test
                accuracy += 1. / (len(Xte)-3000)
                print("Calculation completed ! ! ")
                print(K,"-th neighbors' Accuracy is:",accuracy)
    writer = tf.summary.FileWriter('./graphs', sess.graph)
    writer.close()
    # tensorboard --logdir="./graphs" --port 6006 in command promt to see the graph at localhost:6006
 


